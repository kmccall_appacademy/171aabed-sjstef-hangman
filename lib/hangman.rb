file = File.join(File.dirname(File.expand_path(__FILE__)), './dictionary.txt')
dictionary = File.open(file)
dictionary.readlines{|line| @dictionary.push(line)}


class Hangman
  attr_reader :guesser, :referee, :board
  
  def initialize(options = {})
      default = {
        guesser: HumanPlayer.new(name="human"),
        referee: ComputerPlayer.new
        }
      options = default.merge(options)
        
      @guesser = options[:guesser]
      @referee = options[:referee]
  end
  
  def setup
      @secret_word = @referee.pick_secret_word
      @board = ["_"] * @secret_word
      @referee.register_secret_length(@secret_word)
  end
  
  def take_turn
      p @board
      guess = @guesser.guess(board)
      @referee.update(board, guess)
  end
  
  def play
    self.setup
    count = 10
    until count == 0 || @board.include?("_") == false
      puts "You have #{count} turns remaining"
      turn_start = board.count("_")
      take_turn
      turn_end = board.count("_")
      count = count - 1 unless turn_start > turn_end
    end
    
    if count == 0
        puts "I'm sorry, you have run out of turns. The secret word was #{@referee.word}"
    else
        puts "Congratulations! You've guessed the secret word!"
    end
  end

end

class HumanPlayer
    attr_reader :word
    
    def initialize(name)
        @name = name
    end
    
    def guess(board)
        puts "Please guess a letter"
        @guess = gets.chomp.downcase
    end
    
    def register_secret_length(length)
        puts "The secret word has #{length} letters in it"
    end
    
    def pick_secret_word
        puts "How long is your word?"
        @word = gets.chomp.to_i
    end
    
    def update(board, letter)
        puts "Does your word contain the letter #{letter}?"
        correct_guess = gets.chomp
        if correct_guess == "yes"
            puts "At what positions does the letter exist?"
            idxs = gets.chomp.split(',')
            idxs.each{|idx| board[idx.to_i-1] = letter}
        else
            board
        end
        board
    end
    
end

class ComputerPlayer
    attr_reader :word
    
    def initialize(dictionary=['dictionary', 'add', 'foo', 'bar', 'getaway'])
        @dictionary = dictionary
        @guesses = []
    end
    
    def pick_secret_word
        @word = @dictionary.shuffle[0]
        return @word.length
    end
    
    def check_guess(letter)
        idxs = Array.new
        if @word.include?(letter)
            idxs = @word.chars.each_index.select{|idx| @word[idx] == letter}
            puts "The word contains #{idxs.length} #{letter}'s"
        else
            puts "Sorry you have guessed incorrectly"
            idxs
        end
        idxs
    end
    
    def candidate_word(board)
        length = board.length
        possible_words = @dictionary.select{|word| word.length}
        possible_words.select do |word|
            board.each_with_index do |letter, idx|
                word[idx] == letter unless letter == "_"
            end
        end
        possible_words
    end
    
    def guess(board)
        best_words = self.candidate_word(board)
        letters = Hash.new(0)
        best_words.each do |word|
            word.chars.each{|ltr| letters[ltr] += 1 unless @guesses.include?(ltr)}
        end
        guess = letters.key(letters.values.max)
        @guesses << guess
        guess
    end
    
    def update(board, letter)
        idxs = self.check_guess(letter)
        if idxs.any? 
            idxs.each{|idx| board[idx] = letter }
        end
        board
    end
    
    def register_secret_length(length)
        puts "The secret word has #{length} letters in it"
    end
    
    def update_letters_choices(guess)
    end
end
